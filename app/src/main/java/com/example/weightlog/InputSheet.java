package com.example.weightlog;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InputSheet extends AppCompatActivity {

    private EditText et_weight;
    private DB_connect helper;
    private SQLiteDatabase db;
    private String datetime;
    private TextView debug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_sheet);

        //変数の設定
        final Button button_OK = findViewById(R.id.b_input_OK);
        final Button button_NG = findViewById(R.id.b_input_NG);
        et_weight = findViewById(R.id.et_input_weight);
        et_weight.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        //OKボタン処理
        button_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //DBのインスタンス化
                if(helper == null){
                    helper = new DB_connect(getApplicationContext());
                }
                //
                if(db == null){
                    db = helper.getWritableDatabase();
                }

                //Edittext内のデータ取得
                String weight = et_weight.getText().toString();

                //dbにデータ挿入関数実行
                insertData(db, Double.valueOf(weight));

                    //Main画面に戻る
                Intent intent_OK = new Intent(InputSheet.this, MainActivity.class);
                startActivity(intent_OK);
            }
        });

        //NGボタン処理
        button_NG.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                Intent intent_NG = new Intent(InputSheet.this, MainActivity.class);
                startActivity(intent_NG);

//                readData();
            }
        });
    }

    private void insertData(SQLiteDatabase db, double weight){
        ContentValues values = new ContentValues();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String date = sdf.format(new Date());
        values.put("weight", weight + "kg");
        values.put("datetime", date);

        db.insert("t_weight", null, values);
    }

    private void readData(){
        if(helper == null){
            helper = new DB_connect(getApplicationContext());
        }
        if(db == null){
            db = helper.getReadableDatabase();
        }

        Cursor c = db.rawQuery("select datetime, weight from t_weight", null);

        c.moveToFirst();
        StringBuilder sbuilder = new StringBuilder();

        for (int i = 0; i < c.getCount(); i++){
            sbuilder.append(c.getString(0));
            sbuilder.append(": ");
            sbuilder.append(c.getInt(1));
            sbuilder.append("\n");
            c.moveToNext();
        }

        c.close();
        debug.setText(sbuilder.toString());
    }
}
