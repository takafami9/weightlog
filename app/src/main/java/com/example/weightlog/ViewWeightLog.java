package com.example.weightlog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ViewWeightLog extends AppCompatActivity {

    private TextView view;
    private DB_connect helper;
    private SQLiteDatabase db;
    private Button b_next;
    private Button b_back;
    private int lownum = 1;
    private int highnum = 20;
    private String low = Integer.toString(lownum);
    private String high = Integer.toString(highnum);
    private String sql;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_weight_log);

        //変数の設定
        Button button_back = findViewById(R.id.button5);
        b_next = findViewById(R.id.log_next);
        b_back = findViewById(R.id.log_back);
        view = findViewById(R.id.tv_view);

        sql = "with sub as (select (select count(*) from t_weight w2 where w2._id >= w1._id) number" +
                ", datetime, weight from t_weight w1 order by _id desc) " +
                "select * from sub where number < 21";

        white_sql(sql);

        b_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lownum > 20){
                    lownum -= 20;
                    highnum -= 20;
                    low = Integer.toString(lownum);
                    high = Integer.toString(highnum);

                    sql = "with sub as (select (select count(*) from t_weight w2 where w2._id >= w1._id) number" +
                            ", datetime, weight from t_weight w1 order by _id desc) " +
                            "select * from sub where number <= " + high + " and number >= " + low;
                    white_sql(sql);
                }
            }
        });

        b_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lownum += 20;
                highnum += 20;
                low = Integer.toString(lownum);
                high = Integer.toString(highnum);

                sql = "with sub as (select (select count(*) from t_weight w2 where w2._id >= w1._id) number" +
                        ", datetime, weight from t_weight w1 order by _id desc) " +
                        "select * from sub where number <= " + high + " and number >= " + low;
                white_sql(sql);
            }
        });

        //戻るボタン処理
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_back = new Intent (ViewWeightLog.this, MainActivity.class);
                startActivity(intent_back);
            }
        });
    }

    private void white_sql(String sql) {
        if(helper == null){
            helper = new DB_connect(getApplicationContext());
        }
        if(db == null){
            db = helper.getReadableDatabase();
        }

        Cursor c = db.rawQuery(sql, null);

        c.moveToFirst();
        StringBuilder sbuilder = new StringBuilder();

        for (int i = 0; i < c.getCount(); i++){
            sbuilder.append(c.getString(0));
            sbuilder.append("\t ");
            sbuilder.append(c.getString(1));
            sbuilder.append("\t ");
            sbuilder.append(c.getString(2));
            sbuilder.append("\n");
            c.moveToNext();
        }

        c.close();
        view.setText(sbuilder.toString());
    }

}
