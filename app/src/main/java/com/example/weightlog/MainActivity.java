package com.example.weightlog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private DB_connect helper;
    private SQLiteDatabase db;
    private TextView date;
    private TextView weight;
    private String sql;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //変数の設定
        final Button button_input = findViewById(R.id.b_main_imput);
        final Button button_view = findViewById(R.id.b_main_show);
        date = findViewById(R.id.tv_main_date);
        weight = findViewById(R.id.tv_main_weight);

        //テキストボックスに数値を代入
//        readDate();
//        readWeight();
        sql = "with sub as (select (select count(*) from t_weight w2 where w2._id >= w1._id) number" +
                ", datetime, weight from t_weight w1 order by _id desc) " +
                "select * from sub where number = 1";
        write_sql(sql);


        //入力画面に移動
        button_input.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent_input = new Intent(MainActivity.this, InputSheet.class);
                startActivity(intent_input);
            }
        });

        //体重一覧画面に移動
        button_view.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent_view = new Intent(MainActivity.this, ViewWeightLog.class);
                startActivity(intent_view);
            }
        });
    }

    private void write_sql(String sql){
        if(helper == null){
            helper = new DB_connect(getApplicationContext());
        }
        if(db == null){
            db = helper.getReadableDatabase();
        }

        Cursor c = db.rawQuery(sql, null);

        c.moveToFirst();
//        StringBuilder sbuilder = new StringBuilder();

        date.append(c.getString(1));
        weight.append(c.getString(2));
        c.close();
        }
}
